// src/components/NoteList.js
import React, { useEffect, useState } from 'react';
import axios from 'axios';

const NoteList = () => {
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    // Realiza una solicitud GET a la API para obtener las notas cuando se monta el componente
    axios.get('http://localhost:3000/api/note')
      .then((response) => {
        setNotes(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener las notas:', error);
      });
  }, []);

  return (
    <div>
      <h2>Lista de Notas</h2>
      <ul>
        {notes.map((note) => (
          <li key={note._id}>
            <h3>{note.title}</h3>
            <p>{note.text}</p>
            <p>¿Hecho? {note.noteDone ? 'Sí' : 'No'}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default NoteList;
