import React, { useState } from 'react';
import './App.css';
import NoteList from './components/NoteList';
import NoteModal from './components/NoteModal';

function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Mi Aplicación de Notas</h1>
        <button onClick={openModal}>Agregar Nota</button>
      </header>
      <main>
        <NoteList />
      </main>
      <NoteModal isOpen={isModalOpen} onRequestClose={closeModal} />
    </div>
  );
}

export default App;